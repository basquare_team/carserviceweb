﻿using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CarService.DAL
{
    public class CarRepository : IDBOperations<Car>
    {
        public IEnumerable<Car> GetAll()
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Cars
                    .Include(c => c.Category)
                    .Include(c => c.Fuel)
                    .ToList();
            }
        }

        public Car GetById(int id)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Cars
                    .Include(c => c.Category)
                    .Include(c => c.Fuel)
                    .FirstOrDefault(c => c.Id == id);
            }
        }

        public Car Update(Car item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Car car = context.Cars
                    .Include(c => c.Category)
                    .Include(c => c.Fuel)
                    .FirstOrDefault(c => c.Id == item.Id);

                car.CategoryId = item.CategoryId;
                car.FuelId = item.FuelId;
                car.Mark = item.Mark;
                car.Model = item.Model;
                //car.MarkId = item.MarkId;
                //car.ModelId = item.ModelId;
                car.VIN = item.VIN;
                car.Note = item.Note;

                car.LastEditDateTime = DateTime.UtcNow;

                context.SaveChanges();

                return car;
            }
        }

        public Car Create(Car item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Car car = new Car()
                {
                    CategoryId = item.CategoryId,
                    FuelId = item.FuelId,
                    //MarkId = item.MarkId,
                    //ModelId = item.ModelId,
                    Mark = item.Mark,
                    Model = item.Model,
                    VIN = item.VIN,
                    Note = item.Note,

                    AddDateTime = DateTime.UtcNow
                };

                car = context.Cars.Add(car);

                context.SaveChanges();

                return car;
            }
        }
    }
}
