﻿using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService.DAL
{
    public class FuelRepository : IDBOperations<Fuel>
    {
        public IEnumerable<Fuel> GetAll()
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Fuels.ToList();
            }
        }

        public Fuel GetById(int id)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Fuels.FirstOrDefault(f => f.Id == id);
            }
        }

        public Fuel Update(Fuel item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Fuel fuelType = context.Fuels.FirstOrDefault(f => f.Id == item.Id);

                fuelType.Name = item.Name;

                context.SaveChanges();

                return fuelType;
            }
        }

        public Fuel Create(Fuel item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Fuel fuelType = new Fuel()
                {
                    Name = item.Name
                };

                fuelType = context.Fuels.Add(fuelType);

                context.SaveChanges();

                return fuelType;
            }
        }
    }
}