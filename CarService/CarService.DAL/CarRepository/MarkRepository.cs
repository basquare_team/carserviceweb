﻿using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService.DAL
{
    public class MarkRepository : IDBOperations<Mark>
    {
        public IEnumerable<Mark> GetAll()
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Marks.ToList();
            }
        }

        public Mark GetById(int id)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Marks.FirstOrDefault(m => m.Id == id);
            }
        }

        public Mark Update(Mark item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Mark mark = context.Marks.FirstOrDefault(m => m.Id == item.Id);

                mark.Name = item.Name;

                context.SaveChanges();

                return mark;
            }
        }

        public Mark Create(Mark item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Mark mark = new Mark()
                {
                    Name = item.Name
                };

                mark = context.Marks.Add(mark);

                context.SaveChanges();

                return mark;
            }
        }
    }
}