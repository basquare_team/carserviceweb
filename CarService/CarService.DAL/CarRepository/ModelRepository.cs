﻿using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService.DAL
{
    public class ModelRepository : IDBOperations<Model>
    {
        public IEnumerable<Model> GetAll()
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Models.ToList();
            }
        }

        public Model GetById(int id)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Models.FirstOrDefault(m => m.Id == id);
            }
        }

        public Model Update(Model item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Model model = context.Models.FirstOrDefault(m => m.Id == item.Id);

                model.Name = item.Name;

                context.SaveChanges();

                return model;
            }
        }

        public Model Create(Model item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Model model = new Model()
                {
                    Name = item.Name
                };

                model = context.Models.Add(model);

                context.SaveChanges();

                return model;
            }
        }
    }
}