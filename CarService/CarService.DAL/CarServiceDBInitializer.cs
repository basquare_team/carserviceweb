﻿using CarService.DAL;
using CarService.Models;
using System.Data.Entity;

namespace CarService.DAL
{
    public class CarServiceDBInitializer<T> : DropCreateDatabaseIfModelChanges<CarServiceDbContext>
    {
        protected override void Seed(CarServiceDbContext context)
        {
            CategoryRepository catRepo = new CategoryRepository();
            Category cat1 = catRepo.Create(new Category() { Name = "Moto" });
            Category cat2 = catRepo.Create(new Category() { Name = "Car" });
            catRepo.Create(new Category() { Name = "Truck" });

            FuelRepository fuelRepo = new FuelRepository();
            Fuel fuel1 = fuelRepo.Create(new Fuel() { Name = "Petrol" });
            Fuel fuel2 = fuelRepo.Create(new Fuel() { Name = "Disel" });
            fuelRepo.Create(new Fuel() { Name = "LPG" });
            fuelRepo.Create(new Fuel() { Name = "Electric" });

            EventTypesRepository eventTypesRepository = new EventTypesRepository();
            EventType eventType1 = eventTypesRepository.Create(new EventType() { Name = "Service" });
            EventType eventType2 = eventTypesRepository.Create(new EventType() { Name = "Repair" });
            eventTypesRepository.Create(new EventType() { Name = "Engine failure" }); 
            eventTypesRepository.Create(new EventType() { Name = "Replacement of glass" });

            ServiceRepository serviceRepo = new ServiceRepository();
            Service service1 = serviceRepo.Create(new Service() { Name = "Service 1", Address = "Address 1", Description = "Description 1" });
            Service service2 = serviceRepo.Create(new Service() { Name = "Service 2", Address = "Address 2", Description = "Description 2" });
            serviceRepo.Create(new Service() { Name = "Service 3", Address = "Address 3", Description = "Description 3" });

            PartRepository partRepo = new PartRepository();
            Part part1 = partRepo.Create(new Part() { Name = "Part 1", Price = 1, SerialNumber = " Serial 1", Description = "Description 1" });
            Part part2 = partRepo.Create(new Part() { Name = "Part 2", Price = 2, SerialNumber = " Serial 2", Description = "Description 2" });
            partRepo.Create(new Part() { Name = "Part 3", Price = 3, SerialNumber = " Serial 3", Description = "Description 3" });

            CarRepository carRepo = new CarRepository();
            carRepo.Create(new Car() { CategoryId = cat1.Id, FuelId = fuel1.Id, Mark = "Ford", Model = "Fiesta", Note = "Car1", VIN ="Some VIN1" });
            carRepo.Create(new Car() { CategoryId = cat2.Id, FuelId = fuel2.Id, Mark = "Toyota", Model = "Tundra", Note = "Car2", VIN = "Some VIN2" });

            base.Seed(context);
        }
    }
}