﻿using CarService.Models;
using System.Data.Entity;
using System.Linq;

namespace CarService.DAL
{
    public class CarServiceDbContext : DbContext
    {
        public static CarServiceDbContext Create()
        {
            return new CarServiceDbContext();
        }

        public CarServiceDbContext() : base("CarService")
        {
            Database.SetInitializer<CarServiceDbContext>(new CarServiceDBInitializer<CarServiceDbContext>());
        }

        public DbSet<Mark> Marks { get; set; }
        public DbSet<Model> Models { get; set; }

        public DbSet<Car> Cars { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Fuel> Fuels { get; set; }

        public DbSet<Service> Services { get; set; }

        public DbSet<Part> Parts { get; set; }

        public DbSet<Event> Events { get; set; }
        public DbSet<EventType> EventTypes { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Event>()
                .HasMany<Part>(s => s.Parts)
                .WithMany(c => c.Events)
                .Map(cs =>
                {
                    cs.MapLeftKey("EventId");
                    cs.MapRightKey("PartId");
                    cs.ToTable("EventToPart");
                });
        }
    }
}
