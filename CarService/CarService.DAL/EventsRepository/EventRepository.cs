﻿using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace CarService.DAL
{
    public class EventRepository : IDBOperations<Event>
    {
        public Event Create(Event item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Event eventItem = new Event()
                {
                    CarId = item.CarId,
                    Description = item.Description,
                    EventTypeId = item.EventTypeId,
                    RepairDate = item.RepairDate,
                    FailureDate = item.FailureDate,
                    ServiceId = item.ServiceId,

                    //Parts = item.Parts
                };

                List<Part> partsEntity = new List<Part>();


                foreach (Part part in item.Parts)
                {
                    partsEntity.Add(context.Parts.First(p => p.Id == part.Id));
                }

                eventItem.Parts = partsEntity;

                eventItem = context.Events.Add(eventItem);

                context.SaveChanges();

                return eventItem;
            }
        }

        public IEnumerable<Event> GetAll()
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Events
                    .Include(e => e.Service)
                    .Include(e => e.EventType)
                    .Include(e => e.Car)
                    .Include(e => e.Parts)
                    .ToList();
            }
        }

        public Event GetById(int id)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Events
                    .Include(e => e.Parts)
                    .FirstOrDefault(e => e.Id == id);
            }
        }

        public Event Update(Event item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Event eventItem = context.Events.FirstOrDefault(e => e.Id == item.Id);

                eventItem.CarId = item.CarId;
                eventItem.Description = item.Description;
                eventItem.EventTypeId = item.EventTypeId;
                eventItem.RepairDate = item.RepairDate;
                eventItem.FailureDate = item.FailureDate;
                eventItem.ServiceId = item.ServiceId;

                eventItem.Parts = item.Parts;

                context.SaveChanges();

                return eventItem;
            }
        }
    }
}