﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService.DAL
{
    public interface IDBOperations<T>
    {
        IEnumerable<T> GetAll();

        T GetById(int id);
        T Create(T item);
        T Update(T item);

        //bool Delete();
    }
}