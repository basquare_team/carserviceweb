﻿using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService.DAL
{
    public class PartRepository : IDBOperations<Part>
    {
        public Part Create(Part item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Part part = new Part()
                {
                    Name = item.Name,
                    Description = item.Description,
                    SerialNumber = item.SerialNumber,
                    Price = item.Price
                };

                part = context.Parts.Add(part);

                context.SaveChanges();

                return part;
            }
        }

        public IEnumerable<Part> GetAll()
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Parts.ToList();
            }
        }

        public Part GetById(int id)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Parts.FirstOrDefault(p => p.Id == id);
            }
        }

        public Part Update(Part item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Part part = context.Parts.FirstOrDefault(p => p.Id == item.Id);

                part.Name = item.Name;
                part.SerialNumber = item.SerialNumber;
                part.Price = item.Price;
                part.Description = item.Description;

                context.SaveChanges();

                return part;
            }
        }
    }
}