﻿using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService.DAL
{
    public class ServiceRepository : IDBOperations<Service>
    {
        public Service Create(Service item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Service service = new Service()
                {
                    Name = item.Name,
                    Address = item.Address,
                    Description = item.Description
                };

                service = context.Services.Add(service);

                context.SaveChanges();

                return service;
            }
        }

        public IEnumerable<Service> GetAll()
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Services.ToList();
            }
        }

        public Service GetById(int id)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Services.FirstOrDefault(s => s.Id == id);
            }
        }

        public Service Update(Service item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Service service = context.Services.FirstOrDefault(s => s.Id == item.Id);

                service.Name = item.Name;
                service.Address = item.Address;
                service.Description = item.Description;

                context.SaveChanges();

                return service;
            }
        }
    }
}