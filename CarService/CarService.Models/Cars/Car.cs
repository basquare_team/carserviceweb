﻿using System;

namespace CarService.Models
{
    public class Car
    {
        public int Id { get; set; }

        public DateTime AddDateTime { get; set; }
        public DateTime? LastEditDateTime { get; set; }
        public string Note { get; set; }
        public string VIN { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public string Mark { get; set; }
        public string Model { get; set; }

        //public int MarkId { get; set; }
        //public virtual Mark Mark { get; set; }

        //public int ModelId { get; set; }
        //public virtual Model Model { get; set; }

        public int FuelId { get; set; }
        public virtual Fuel Fuel { get; set; }
    }
}
