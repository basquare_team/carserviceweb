﻿namespace CarService.Models
{
    public class Mark
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
