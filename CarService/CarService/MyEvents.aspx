﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyEvents.aspx.cs" Inherits="CarService.MyEvents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>My Events</h1>
    <asp:GridView ID="MyEvents_GridView" runat="server"></asp:GridView>
    <hr />
     
    <h4>Add Events</h4>
    <div class="width-300">
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="CarsLabel" runat="server" Text="Car"></asp:Label></div>
            <div class="col-xs-6"><asp:DropDownList Width="150" ID="CarsDropDownList" runat="server"></asp:DropDownList></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="EventTypesLabel" runat="server" Text="Event type"></asp:Label></div>
            <div class="col-xs-6"><asp:DropDownList Width="150" ID="EventTypesDropDownList" runat="server"></asp:DropDownList></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="ServiceLabel" runat="server" Text="Service"></asp:Label></div>
            <div class="col-xs-6"><asp:DropDownList Width="150" ID="ServiceDropDownList" runat="server"></asp:DropDownList></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="PartsLabel" runat="server" Text="Parts"></asp:Label></div>
            <div class="col-xs-6"><asp:ListBox Width="150" runat="server" SelectionMode="Multiple" ID="PartsListBox"></asp:ListBox></div>
        </div>

        <div class="row">
            <div class="col-xs-6"><asp:Label ID="DescriptionLabel" runat="server" Text="Description"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox Width="150" ID="DescriptionTextBox" runat="server"></asp:TextBox></div>
        </div>
        <center>
            <asp:Button ID="AddEventButton" OnClick="AddEventButton_Click" runat="server" Text="Save" />
        </center>
    </div>
</asp:Content>


