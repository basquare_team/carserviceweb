﻿using CarService.DAL;
using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarService
{
    public partial class Parts : System.Web.UI.Page
    {
        private PartRepository _partRepo;

        protected void Page_Load(object sender, EventArgs e)
        {
            _partRepo = new PartRepository();

            Parts_GridView.DataSource = _partRepo.GetAll();
            Parts_GridView.DataBind();
        }

        protected void Parts_GridView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true;

            AddPartButton.Visible = false;
            UpdatePartButton.Visible = true;
            ClearPartFormButton.Visible = true;

            string columnIdValue = Parts_GridView.Rows[e.NewEditIndex].Cells[1].Text;

            int serviceForEdit = Convert.ToInt32(columnIdValue);

            //load service for edit
            Part part = _partRepo.GetById(serviceForEdit);

            PartIdLabel.Text = part.Id.ToString();

            NameTextBox.Text = part.Name;
            SerialNumberTextBox.Text = part.SerialNumber;
            PriceTextBox.Text = part.Price.ToString();
            DescriptionTextBox.Text = part.Description;
        }

        protected void AddPartButton_Click(object sender, EventArgs e)
        {
            Part part = new Part()
            {
                Name = NameTextBox.Text,
                SerialNumber = SerialNumberTextBox.Text,
                Price = Convert.ToDouble(PriceTextBox.Text),
                Description = DescriptionTextBox.Text
            };

            part = _partRepo.Create(part);

            Parts_GridView.DataSource = _partRepo.GetAll();
            Parts_GridView.DataBind();

            clearEditForm();
        }

        protected void UpdatePartButton_Click(object sender, EventArgs e)
        {
            Part part = new Part()
            {
                Id = Convert.ToInt32(PartIdLabel.Text),
                Name = NameTextBox.Text,
                SerialNumber = SerialNumberTextBox.Text,
                Price = Convert.ToDouble(PriceTextBox.Text),
                Description = DescriptionTextBox.Text
            };

            part = _partRepo.Update(part);

            Parts_GridView.DataSource = _partRepo.GetAll();
            Parts_GridView.DataBind();

            AddPartButton.Visible = true;
            UpdatePartButton.Visible = false;
            ClearPartFormButton.Visible = false;

            clearEditForm();
        }

        protected void ClearPartFormButton_Click(object sender, EventArgs e)
        {
            clearEditForm();

            AddPartButton.Visible = true;
            UpdatePartButton.Visible = false;
            ClearPartFormButton.Visible = false;
        }

        private void clearEditForm()
        {
            NameTextBox.Text = "";
            SerialNumberTextBox.Text = "";
            PriceTextBox.Text = "";
            DescriptionTextBox.Text = "";
        }
    }
}