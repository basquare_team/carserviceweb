﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Services.aspx.cs" Inherits="CarService.Services" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Services</h1>
    <asp:GridView ID="Services_GridView" runat="server" OnRowEditing="Services_GridView_RowEditing"  AutoGenerateEditButton="true"></asp:GridView>
    <hr />

    <asp:Label ID="ServiceIdLabel" runat="server" Text="" Visible="false"></asp:Label>
    <div class="width-300">
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="NameLabel" runat="server" Text="Name"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="NameTextBox" runat="server"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="AddressLabel" runat="server" Text="Address"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="AddressTextBox" runat="server"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="DescriptionLabel" runat="server" Text="Description"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="DescriptionTextBox" runat="server"></asp:TextBox></div>
        </div>
        <center>
            <asp:Button ID="AddServiceButton" OnClick="AddServiceButton_Click" runat="server" Text="Save" />
            <asp:Button ID="UpdateServiceButton" OnClick="UpdateServiceButton_Click" runat="server" Text="Update" Visible ="false" />
            <asp:Button ID="ClearServiceFormButton" OnClick="ClearServiceFormButton_Click" runat="server" Text="Clear" Visible ="false" />
        </center>    
    </div>
</asp:Content>
