﻿using CarService.DAL;
using CarService.Models;
using System;
using System.Web.UI.WebControls;

namespace CarService
{
    public partial class Services : System.Web.UI.Page
    {
        private ServiceRepository _serviceRepo;

        protected void Page_Load(object sender, EventArgs e)
        {
            _serviceRepo = new ServiceRepository();

            Services_GridView.DataSource = _serviceRepo.GetAll();
            Services_GridView.DataBind();
        }

        protected void Services_GridView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true;

            AddServiceButton.Visible = false;
            UpdateServiceButton.Visible = true;
            ClearServiceFormButton.Visible = true;

            string columnIdValue = Services_GridView.Rows[e.NewEditIndex].Cells[1].Text;

            int serviceForEdit = Convert.ToInt32(columnIdValue);

            //load service for edit
            Service service = _serviceRepo.GetById(serviceForEdit);

            ServiceIdLabel.Text = service.Id.ToString();

            NameTextBox.Text = service.Name;
            AddressTextBox.Text = service.Address;
            DescriptionTextBox.Text = service.Description;
        }

        protected void AddServiceButton_Click(object sender, EventArgs e)
        {
            Service service = new Service()
            {
                Name = NameTextBox.Text,
                Address = AddressTextBox.Text,
                Description = DescriptionTextBox.Text
            };

            service = _serviceRepo.Create(service);

            Services_GridView.DataSource = _serviceRepo.GetAll();
            Services_GridView.DataBind();

            clearEditForm();
        }

        protected void UpdateServiceButton_Click(object sender, EventArgs e)
        {
            Service service = new Service()
            {
                Id = Convert.ToInt32(ServiceIdLabel.Text),
                Name = NameTextBox.Text,
                Address = AddressTextBox.Text,
                Description = DescriptionTextBox.Text
            };

            service = _serviceRepo.Update(service);

            Services_GridView.DataSource = _serviceRepo.GetAll();
            Services_GridView.DataBind();

            AddServiceButton.Visible = true;
            UpdateServiceButton.Visible = false;
            ClearServiceFormButton.Visible = false;

            clearEditForm();
        }

        protected void ClearServiceFormButton_Click(object sender, EventArgs e)
        {
            clearEditForm();

            AddServiceButton.Visible = true;
            UpdateServiceButton.Visible = false;
            ClearServiceFormButton.Visible = false;
        }
        

        private void clearEditForm()
        {
            NameTextBox.Text = "";
            AddressTextBox.Text = "";
            DescriptionTextBox.Text = "";
        }
    }
}