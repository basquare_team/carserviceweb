﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StaticData.aspx.cs" Inherits="CarService.StaticData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row width-300">
        <div class="col-xs-6">
            <asp:GridView ID="Categories_GridView" runat="server"></asp:GridView>
        </div>
        <div class="col-xs-6">
            <asp:GridView ID="Fuels_GridView" runat="server"></asp:GridView>
        </div>
    </div>

    <asp:GridView ID="EventTypes_GridView" runat="server"></asp:GridView>

</asp:Content>