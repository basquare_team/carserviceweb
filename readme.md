## Car service

# Setup project locally

All is needed to setup the project locally is to have Visual Studio installed on a computer. 

# Project description

The website contains the following pages: 
•	My cars (the same as home page) – the list of cars, that user owns;
•	Services – the list of car service stations where user can repair his cars;
•	Parts – the list of spare parts of the car;
•	My events – service history of the car;
•	Static data – information that can’t be changed and is used to specify the type of car, fuel, services. 

# My cars page

Using that page user can add a new car to the list or make the adjustments to the exited car specification. While adding a new car user can define the type of a car, type of fuel, mark, model, VIN number and add some note about the car. 

# Services

With the help of Services page user can create and update the list of car service stations. While creating a new list item user can define service name, address and add some description. 

# Parts

On that page user can list the spare parts of the car he once replaced. Updating item in a list is also available. Each item contains parameters: name, serial number, price, description. 

# My events

As mentioned, My events page contains the service history of the car. To add a new item to the list user should specify:
•	Car (from the My cars page only);
•	Event type (one of 4 events from static data);
•	Service (from Parts page only);
•	Description (plain text about the event). 

After the saving a history item will be added to the list. 


